# Services for otter-grader-service Integration Tests

## otter-grader-test-in
Sends 3 submissions to the grading queue:

1. one containing no notebook
2. one containing one notebook
3. one containing two notebooks

Only the second should pass without error, the others should return error codes from
otter_grader_service.grading.worker

## otter-grader-test-out

Acts as receiving service. Evaluates the grading results of the three test submissions.

Reports the current test results back on `/status` as JSON:

```json
{
  "1":1,
  "2":1,
  "3":1,
  "summary":1}
```

The `summary` is set to 1 if all tests have passed.
